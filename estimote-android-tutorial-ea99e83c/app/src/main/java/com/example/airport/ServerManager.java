package com.example.airport;


import android.os.AsyncTask;
import android.util.JsonReader;
import android.util.JsonToken;
import android.util.Log;
import android.util.Xml;


import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;


/**
 * Created by Dor on 24/05/16.
 */
public class ServerManager {
    //http://jsonparsing.parseapp.com/jsonData/moviesData.txt
    private final String URL_TO_HIT = "http://private-55042-beacon13.apiary-mock.com/GetRegion";
    private final String TAG = "Dor's LOG:";


    public void sendPostReq() {

        new JSONTask().execute(URL_TO_HIT);

    }


    public class JSONTask extends AsyncTask<String, String, String>

    {
        @Override
        protected String doInBackground(String... params) {

            HttpURLConnection connection = null;
            BufferedReader reader = null;

            try {


                URL url = new URL(params[0]);
                connection = (HttpURLConnection) url.openConnection();
                connection.setRequestMethod("POST");
                connection.setRequestProperty("Content-Type","application/json");
                connection.connect();

                JSONObject jsonParam = new JSONObject();
                jsonParam.put("appName","Fattal");


                DataOutputStream printout;
                printout = new DataOutputStream(connection.getOutputStream ());
                printout.writeUTF(URLEncoder.encode(jsonParam.toString(), "UTF-8"));
                printout.flush ();
                printout.close ();

                InputStream stream = connection.getInputStream();

                reader = new BufferedReader(new InputStreamReader(stream));
                StringBuffer buffer = new StringBuffer();
                String line = "";


                while ((line = reader.readLine()) != null) {
                    buffer.append(line);
                }
                String result = String.valueOf(buffer);

                JSONObject myObject = new JSONObject(result);
                JsonReader jsonReader = null;

                InputStreamReader inputStreamReader = null;
                inputStreamReader = new InputStreamReader(stream);
                jsonReader = new JsonReader(inputStreamReader);


                return result;

            }

            catch (MalformedURLException e)
            {
                e.printStackTrace();
            }

            catch (IOException e)
            {
                e.printStackTrace();
            }

            catch (JSONException e)

            {
              e.printStackTrace();
            }


            finally {
                if (connection != null) {
                    connection.disconnect();
                }
                try {
                    if (reader != null) {
                        reader.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {

            super.onPostExecute(result);
            Log.i(TAG, result);

        }

    }

}
