package com.example.airport;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.estimote.sdk.Beacon;
import com.estimote.sdk.BeaconManager;
import com.estimote.sdk.Region;
import com.estimote.sdk.SystemRequirementsChecker;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

public class MainActivity extends AppCompatActivity {

    private TextView accuracyText ;

    private TextView uuidText;

    private TextView rssiText ;

    private TextView foundtext ;

    private TextView minorText ;

    private TextView majorText ;

    private TextView distanceText;

    private static final Map<String, List<String>> PLACES_BY_BEACONS;

    private WebView wv1;
    private ServerManager serverManager;


    // TODO: replace "<major>:<minor>" strings to match your own beacons.
    static {
        Map<String, List<String>> placesByBeacons = new HashMap<>();
        placesByBeacons.put("22504:48827", new ArrayList<String>() {{
            add("Heavenly Sandwiches");
            // read as: "Heavenly Sandwiches" is closest
            // to the beacon with major 22504 and minor 48827
            add("Green & Green Salads");
            // "Green & Green Salads" is the next closest
            add("Mini Panini");
            // "Mini Panini" is the furthest away
        }});
        placesByBeacons.put("648:12", new ArrayList<String>() {{
            add("Mini Panini");
            add("Green & Green Salads");
            add("Heavenly Sandwiches");
        }});
        PLACES_BY_BEACONS = Collections.unmodifiableMap(placesByBeacons);
    }

    private List<String> placesNearBeacon(Beacon beacon) {
        String beaconKey = String.format("%d:%d", beacon.getMajor(), beacon.getMinor());
        if (PLACES_BY_BEACONS.containsKey(beaconKey)) {
            return PLACES_BY_BEACONS.get(beaconKey);
        }
        return Collections.emptyList();
    }

    private BeaconManager beaconManager;
    private Region region;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        distanceText = (TextView) findViewById(R.id.distance);
        accuracyText = (TextView) findViewById(R.id.accuracy);
        uuidText = (TextView) findViewById(R.id.uuid);
        rssiText = (TextView) findViewById(R.id.rssi);
        foundtext = (TextView) findViewById(R.id.found);
        minorText =(TextView) findViewById(R.id.minor);
        majorText =(TextView) findViewById(R.id.major);




        foundtext.setText("NO");

        beaconManager = new BeaconManager(this);
        beaconManager.setRangingListener(new BeaconManager.RangingListener() {
            @Override
            public void onBeaconsDiscovered(Region region, List<Beacon> list) {
                if (!list.isEmpty()) {
                    Beacon nearestBeacon = list.get(0);
                    List<String> places = placesNearBeacon(nearestBeacon);
                    // TODO: update the UI here

                    setContentView(R.layout.web_view); // muylayout is your layout.xml
                    WebView webView = (WebView) findViewById(R.id.webView);
                    // returns null pointer
                    webView.loadUrl("http://jsonparsing.parseapp.com/jsonData/moviesDemoItem.txt");

//                    RelativeLayout item = (RelativeLayout)findViewById(R.id.item);
//                    View child = getLayoutInflater().inflate(R.layout.web_view, null);
//                    item.addView(child);

                    Log.d("Airport", "Nearest places: " + places);
                    foundtext.setText("Yes");
                    distanceText.setText(String.valueOf(nearestBeacon.getRssi()));
                    majorText.setText(String.valueOf(nearestBeacon.getMajor()));
                    minorText.setText(String.valueOf(nearestBeacon.getMinor()));




                }
            }
        });
        region = new Region("ranged_region", UUID.fromString("68356073-4B97-4A77-9B94-ED3D65718193"), null, null);
    }

    @Override
    protected void onResume() {
        super.onResume();

        SystemRequirementsChecker.checkWithDefaultDialogs(this);

        beaconManager.connect(new BeaconManager.ServiceReadyCallback() {
            @Override
            public void onServiceReady() {
                beaconManager.startRanging(region);
            }
        });
    }

    @Override
    protected void onPause() {
        serverManager = new ServerManager();

        beaconManager.stopRanging(region);

        serverManager.sendPostReq();


        super.onPause();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
