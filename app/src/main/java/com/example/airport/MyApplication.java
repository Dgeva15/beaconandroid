package com.example.airport;

import android.app.Application;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;

import com.estimote.sdk.Beacon;
import com.estimote.sdk.BeaconManager;
import com.estimote.sdk.Region;
import com.example.airport.Managers.DBHandler;
import com.example.airport.Managers.LocalDataManager;
import com.example.airport.Managers.ServerManager;
import com.example.airport.Models.API;
import com.example.airport.Models.AppDetails;

import java.util.List;
import java.util.UUID;


public class MyApplication extends Application {


    private  LocalDataManager localDataManager;
    private BeaconManager beaconManager;

    @Override
    public void onCreate() {
        super.onCreate();


         ServerManager.getInstance(getApplicationContext(), "GetRegion");



        beaconManager = new BeaconManager(getApplicationContext());
        beaconManager.setMonitoringListener(new BeaconManager.MonitoringListener() {
            @Override
            public void onEnteredRegion(Region region, List<Beacon> list) {

                while (API.ServerResponseData.regionList == null){}
                showNotification("E N T E R Region",API.ServerResponseData.regionList.get(0).getMessage());

            }
            @Override
            public void onExitedRegion(Region region) {
                showNotification
                        ( "Exit region",
                                "o u t " + "o f " + "R e g i o n");  }
        });


        beaconManager.connect(new BeaconManager.ServiceReadyCallback() {
            @Override
            public void onServiceReady() {
                //24CC8428-1FBD-F1C3-CADF-BB378880E914
                //"8F1AD170-84FF-DB97-F126-3B812265F9D4"

                // 68356073-4B97-4A77-9B94-ED3D65718193 dor's beacon uuid

//              while (API.ServerResponseData.regionList == null){}
//                beaconManager.startMonitoring(new Region("monitored region",
//                        UUID.fromString(API.ServerResponseData.regionList.get(0).getRegionId()), null, null));

                beaconManager.startMonitoring(new Region("monitored region",
                        UUID.fromString("68356073-4B97-4A77-9B94-ED3D65718193"), null , null));

            }
        });
    }

    public  void showNotification(String title, String message) {
        Intent notifyIntent = new Intent(this, MainActivity.class);
        notifyIntent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivities(this, 0,
                new Intent[]{notifyIntent}, PendingIntent.FLAG_UPDATE_CURRENT);
        Notification notification = new Notification.Builder(this)
                .setSmallIcon(android.R.drawable.ic_dialog_info)
                .setContentTitle(title)
                .setContentText(message)
                .setAutoCancel(true)
                .setContentIntent(pendingIntent)
                .build();
        notification.defaults |= Notification.DEFAULT_SOUND;
        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(1, notification);
    }



}
