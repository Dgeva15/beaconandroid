package com.example.airport.Managers;


import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;


import com.estimote.sdk.repackaged.gson_v2_3_1.com.google.gson.Gson;
import com.example.airport.Models.API;
import com.example.airport.Models.ServerResponse;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;


/**
 * Created by Dor on 24/05/16.
 */
public class ServerManager  {

    //*************************************Server Manager Singlton creator*********************
    static ServerManager instance;
     Context appContext;


    public static ServerManager getInstance(Context c, String serverRequestName)
    {
        if (instance==null) {
            instance = new ServerManager(c,serverRequestName);
        }
        return  instance;
    }

    private ServerManager (Context context , String serverRequestName)
    {
        appContext=context;
            sendPostReq(serverRequestName);

    }
//**********************************************************
//**********************************************************

    private final String URL_POST_REGIONS = "http://private-55042-beacon13.apiary-mock.com/GetRegion";
    private final String URL_POST_VERSION = "http://private-55042-beacon13.apiary-mock.com/GetAppVersion";

    private final String TAG = "Dor's LOG:";

    private final String BASE_URL = "http://private-55042-beacon13.apiary-mock.com/";
    private static LocalDataManager localDataManager;



    public static API.ServerResponseData serverResponseData;

    public void sendPostReq(String serverRequestName) {

        new JSONTask().execute(BASE_URL+serverRequestName,"2");

    }


    public  class JSONTask extends AsyncTask<String, String, String>

    {
        @Override
        protected String doInBackground(String... params) {

            HttpURLConnection connection = null;
            BufferedReader reader = null;

            switch (params[1]) {
//case 1 get version
                case "1":
                    try {
                    //setting up the request
                    URL url = new URL(params[0]);
                    connection = (HttpURLConnection) url.openConnection();
                    connection.setRequestMethod("POST");
                    connection.setRequestProperty("Content-Type", "application/json");
                    connection.connect();



                        // create json to send to the server
                    JSONObject jsonParam = new JSONObject();
                        jsonParam.put("appName", "test");
                        jsonParam.put("appId","123456");


                    DataOutputStream printout;
                    printout = new DataOutputStream(connection.getOutputStream());
                    printout.writeUTF(URLEncoder.encode(jsonParam.toString(), "UTF-8"));
                    printout.flush();
                    printout.close();

                    //getting the stream from the server


                    InputStream stream = connection.getInputStream();

                    reader = new BufferedReader(new InputStreamReader(stream));
                    StringBuffer buffer = new StringBuffer();
                    String line = "";


                    while ((line = reader.readLine()) != null) {
                        buffer.append(line);
                    }
                    String result = String.valueOf(buffer);


                    JSONObject myObject = new JSONObject(result);

                    Gson gson = new Gson();

                    localDataManager = new LocalDataManager();


                    serverResponseData = new API.ServerResponseData();

                    if (myObject.has("code")) {
                        serverResponseData.setCode(myObject.getInt("code"));

                    }

                    if (myObject.has("message")) {
                        serverResponseData.setMessage(myObject.getString("message"));

                    }
                    if (myObject.has("Data")) {
                        serverResponseData.setData(myObject.getString("Data"));

                    }

                    ServerResponse serverResponse = new ServerResponse();

                    serverResponse.setData(serverResponseData.getData());
                    serverResponse.setCode(serverResponseData.getCode());
                    serverResponse.setMessage(serverResponseData.getMessage());


                    //gets all the Data from the server
                        LocalDataManager.GetAppVersion getAppVersion = gson.fromJson (serverResponseData.getData(), LocalDataManager.GetAppVersion.class);

                    //serverResponseData.setVersion(getAppVersion.version);
                            API.version=getAppVersion.version;



                    LocalDataManager localDataManager = new LocalDataManager();
                    localDataManager.checkForUpdates(appContext ,getAppVersion.version);


                    return result;

                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e)

                {
                    e.printStackTrace();
                } finally {
                    if (connection != null) {
                        connection.disconnect();
                    }
                    try {
                        if (reader != null) {
                            reader.close();
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                    return null;


//case 2 get region
                case "2":
                    try {


                        //setting up the request
                        URL url = new URL(params[0]);
                        connection = (HttpURLConnection) url.openConnection();
                        connection.setRequestMethod("POST");
                        connection.setRequestProperty("Content-Type", "application/json");
                        connection.connect();

                        // create json to send to the server
                        JSONObject jsonParam = new JSONObject();
                        jsonParam.put("appName", "Fattal");


                        DataOutputStream printout;
                        printout = new DataOutputStream(connection.getOutputStream());
                        printout.writeUTF(URLEncoder.encode(jsonParam.toString(), "UTF-8"));
                        printout.flush();
                        printout.close();

                        //getting the stream from the server

                        InputStream stream = connection.getInputStream();

                        reader = new BufferedReader(new InputStreamReader(stream));
                        StringBuffer buffer = new StringBuffer();
                        String line = "";


                        while ((line = reader.readLine()) != null) {
                            buffer.append(line);
                        }
                        String result = String.valueOf(buffer);


//                        JSONObject myObject = new JSONObject(result);
//
//
//                        Gson gson = new Gson();
//
//                        localDataManager = new LocalDataManager();
//
//
//                        serverResponseData = new API.ServerResponseData();
//
//                        if (myObject.has("code")) {
//                            serverResponseData.setCode(myObject.getInt("code"));
//
//                        }
//
//                        if (myObject.has("message")) {
//                            serverResponseData.setMessage(myObject.getString("message"));
//
//                        }
//                        if (myObject.has("Data")) {
//                            serverResponseData.setData(myObject.getString("Data"));
//
//                        }

//                        ServerResponse serverResponse = new ServerResponse();
//
//                        serverResponse.setData(serverResponseData.getData());
//                        serverResponse.setCode(serverResponseData.getCode());
//                        serverResponse.setMessage(serverResponseData.getMessage());


                        //gets all the Data from the server
//                        LocalDataManager.GetRegionsReponse getRegionsReponse = gson.fromJson(serverResponseData.getData(), LocalDataManager.GetRegionsReponse.class);
//
//                        serverResponseData.setRegionList(getRegionsReponse.Regions);
//
//
//                        LocalDataManager localDataManager = new LocalDataManager();
//                        DBHandler db = new DBHandler(appContext);
//                        localDataManager.createRegionsTable(db, getRegionsReponse.Regions);


                        return result;

                    } catch (MalformedURLException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (JSONException e)

                    {
                        e.printStackTrace();
                    } finally {
                        if (connection != null) {
                            connection.disconnect();
                        }
                        try {
                            if (reader != null) {
                                reader.close();
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                    return null;


                //case 3 get beacon
                case "3":

                default:

            }

            return null;
        }



        @Override
        protected void onPostExecute(String result) {


           try {
               JSONObject myObject = new JSONObject(result);

              myObject = this.parseJson(myObject);

           }
            catch (JSONException e)

            {
                e.printStackTrace();
            }

            super.onPostExecute(result);
           // Log.i(TAG, result);
       //    Log.i(TAG, serverResponseData.getData());

        }


        public JSONObject parseJson (JSONObject myObject) {

            try {
                localDataManager = new LocalDataManager();


                serverResponseData = new API.ServerResponseData();

                if (myObject.has("code")) {
                    serverResponseData.setCode(myObject.getInt("code"));

                }

                if (myObject.has("message")) {
                    serverResponseData.setMessage(myObject.getString("message"));

                }
                if (myObject.has("Data")) {
                    serverResponseData.setData(myObject.getString("Data"));

                }

                ServerResponse serverResponse = new ServerResponse();

                serverResponse.setData(serverResponseData.getData());
                serverResponse.setCode(serverResponseData.getCode());
                serverResponse.setMessage(serverResponseData.getMessage());

                JSONObject jsonObjectToReturn = new JSONObject(serverResponseData.getData());

                Gson gson = new Gson();

                LocalDataManager.GetRegionsReponse getRegionsReponse = gson.fromJson(serverResponseData.getData(), LocalDataManager.GetRegionsReponse.class);

                serverResponseData.setRegionList(getRegionsReponse.Regions);


                LocalDataManager localDataManager = new LocalDataManager();
                DBHandler db = new DBHandler(appContext);
                localDataManager.createRegionsTable(db, getRegionsReponse.Regions);

                return jsonObjectToReturn;
            }
            catch (JSONException e)

            {
                e.printStackTrace();
            }

            return myObject;
        }
    }


}
