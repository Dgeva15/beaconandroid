package com.example.airport.Managers;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.airport.Models.API;
import com.example.airport.Models.Region;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Dor on 26/05/16.
 */
public class DBHandler  extends SQLiteOpenHelper {

    // Database Version
     private static final int DATABASE_VERSION = 1;

    // Database Name
    private static final String DATABASE_NAME = "ZiCon";

    // Contacts table name
    private static final String TABLE_regionS = "Regions";
    private static final String TABLE_Version = "Version";

    // Regions Table Columns names
    private static final String KEY_BEACONS = "Beacons";
    private static final String KEY_IDENTIFEIR = "identifier";
    private static final String KEY_MESSAGE = "message";
    private static final String KEY_PINGLINK = "pingLink";
    private static final String KEY_PUSH_INTERVAL= "pushInterval";
    private static final String KEY_REGION_ID = "regionId";
    private static final String KEY_REGION_OPEN_LINK = "regionOpenLink";

    //Version Table Columns names
    private static final String KEY_VERSION = "version";




    public DBHandler(Context context ) {

        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    //=======================DB creation======================
    @Override
    public void onCreate(SQLiteDatabase db) {
        //create region table
        String CREATE_REGIONS_TABLE = "CREATE TABLE " + TABLE_regionS + "("
                + KEY_REGION_ID + " TEXT PRIMARY KEY," + KEY_IDENTIFEIR + " TEXT,"
                + KEY_PINGLINK + " TEXT,"
                + KEY_MESSAGE + " TEXT" +
                KEY_REGION_OPEN_LINK + " TEXT" + ")";
        db.execSQL(CREATE_REGIONS_TABLE);

//create version table
        String CREATE_VERSION_TABLE = "CREATE TABLE " + TABLE_Version + "("
                + KEY_VERSION + " TEXT PRIMARY KEY"+ ")";
        db.execSQL(CREATE_VERSION_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_regionS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_Version);
        // Creating tables again
        onCreate(db);
    }

    //=======================Region Table======================
public boolean insert(String tableName, JSONObject jsonObject )
{
  return true;
}
    // Adding new region
    public void addRegion (Region region) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_IDENTIFEIR, region.getIdentifier()); // region Name
        values.put(KEY_MESSAGE, region.getMessage()); // region Phone Number
        values.put(KEY_REGION_ID, region.getRegionId());
        values.put(KEY_PINGLINK, region.getRegionId());

        // Inserting Row
        db.insert(TABLE_regionS, null, values);
        db.close(); // Closing database connection

    }



    // Getting one region
    public Region getRegion(int id) {

        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(TABLE_regionS, new String[]{KEY_IDENTIFEIR,
                        KEY_MESSAGE, KEY_REGION_ID, KEY_PINGLINK }, KEY_IDENTIFEIR + "=?",
                new String[]{String.valueOf(id)}, null, null, null, null);
        if (cursor != null)
            cursor.moveToFirst();

        Region region = new Region();

        region.setIdentifier(cursor.getString(0));
        region.setIdentifier(cursor.getString(1));
        region.setIdentifier(cursor.getString(2));
        region.setIdentifier(cursor.getString(3));




        // return region
        return region;
    }

    // Getting All regions
    public List<Region> getAllregions() {
        List<Region> regionList = new ArrayList<Region>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_regionS;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Region region = new Region();
                region.setIdentifier(cursor.getString(0));
                region.setIdentifier(cursor.getString(1));
                region.setIdentifier(cursor.getString(2));
                region.setIdentifier(cursor.getString(3));
                // Adding contact to list
                regionList.add(region);
            } while (cursor.moveToNext());
        }

        // return contact list
        return regionList;
    }

    // Getting regions Count
    public int getRegionsCount() {
        String countQuery = "SELECT  * FROM " + TABLE_regionS;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        cursor.close();

        // return count
        return cursor.getCount();
    }

    // Updating a region
    public int updateRegion(Region region) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_IDENTIFEIR, region.getIdentifier());
        values.put(KEY_REGION_ID, region.getRegionId());
        values.put(KEY_MESSAGE, region.getMessage());
        values.put(KEY_MESSAGE, region.getPingLink());


        // updating row
        return db.update(TABLE_regionS, values, KEY_REGION_ID + " = ?",
                new String[]{String.valueOf(region.getRegionId())});
    }

    // Deleting a region
    public void deleteRegion(Region region) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_regionS, KEY_REGION_ID + " = ?",
                new String[] { String.valueOf(region.getRegionId()) });
        db.close();
    }


//============================version table =================================

    //adding new version
    public void addVersion (int version) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_VERSION, version); // region Name

//Todo appdetail udate

        // Inserting Row
        db.insert(TABLE_Version, null, values);
        db.close(); // Closing database connection

    }


    //Getting version
    public int getVersion  () {

        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(TABLE_Version, new String[]{KEY_VERSION }, KEY_VERSION + "=?",
                new String[]{String.valueOf(0)}, null, null, null, null);
        if (cursor != null)
            cursor.moveToFirst();


        API.version =(Integer.parseInt(cursor.getString(0)));



        // return version
        return API.version;
    }

    // Updating a version
    public int updateVersion(int version) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_VERSION, version);


        // updating row
        return db.update(TABLE_Version, values, KEY_VERSION + " = ?",
                new String[]{String.valueOf(version)});
    }

    // Deleting a version
    public void deleteVersion (int version) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_Version, KEY_VERSION + " = ?",
                new String[] { String.valueOf(version) });
        db.close();
    }

    public int getVersionsCount() {
        String countQuery = "SELECT  * FROM " + TABLE_Version;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        cursor.close();

        // return count
        return cursor.getCount();
    }

}
