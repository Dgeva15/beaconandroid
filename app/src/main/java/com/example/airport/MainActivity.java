package com.example.airport;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.estimote.sdk.Beacon;
import com.estimote.sdk.BeaconManager;
import com.estimote.sdk.Region;
import com.estimote.sdk.SystemRequirementsChecker;
import com.example.airport.Models.API;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

public class MainActivity extends AppCompatActivity {


  private static final Map<String, List<String>> PLACES_BY_BEACONS;
   public int timer =0;



    // TODO: replace "<major>:<minor>" strings to match your own beacons.
 static {

        Map<String, List<String>> placesByBeacons = new HashMap<>();

        placesByBeacons.put( "16:16",
             new ArrayList<String>() {{
            add("Zigit Beacon 1");
            // read as: "Zigit Beacon 1" is closest
           // to the beacon with major 22504 and minor 48827
            add("Zigit Beacon 2");
           // "Zigit Beacon 2" is the next closest
           add("Zigit Beacon 3");
            // "Zigit Beacon 3" is the furthest away
       }});

       placesByBeacons.put("10009:22364" +
               "",
               new ArrayList<String>() {{
                   add("Zigit Beacon 3");
                   add("Zigit Beacon 2");
                   add("Zigit Beacon 1");
               }});
       PLACES_BY_BEACONS = Collections.unmodifiableMap(placesByBeacons);

    }

    private List<String> placesNearBeacon (Beacon beacon) {

        String beaconKey = String.format("%d:%d", beacon.getMajor(), beacon.getMinor());

        if (PLACES_BY_BEACONS.containsKey(beaconKey)) {
            return PLACES_BY_BEACONS.get(beaconKey);
        }
        return Collections.emptyList();
    }

    private BeaconManager beaconManager;
    private Region region;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        beaconManager = new BeaconManager(this);
        beaconManager.setRangingListener(new BeaconManager.RangingListener()

        {
            @Override
            public void onBeaconsDiscovered(Region region, List<Beacon> list) {
                if (!list.isEmpty()) {
                    Beacon nearestBeacon = list.get(0);
                   List<String> places = placesNearBeacon(nearestBeacon);
                    // update the UI here every beacon discover

                    if (timer == 0) {
                        RelativeLayout item = (RelativeLayout) findViewById(R.id.item);
                        View child = getLayoutInflater().inflate(R.layout.web_view, null);

                        item.addView(child);

                    }

                    timer++;

                    if (timer==100)
                    {
                        timer=0;
                    }


                    final Button button = (Button) findViewById(R.id.button);
                    button.setOnClickListener(new View.OnClickListener() {
                        public void onClick(View v)
                        {

                            RelativeLayout item = (RelativeLayout)findViewById(R.id.item);

                            View main = getLayoutInflater().inflate(R.layout.activity_main, null);
                            item.addView(main);



                        }
                    });




                   Log.i("ZiCon", "Nearest places: " + places);
                    showNotification( "Nearest places:" , places.get(0)+" "+places.get(1)+" "+places.get(2));

                }
            }
        });

        //UUID.fromString(API.ServerResponseData.regionList.get(0).getRegionId())
        //while (API.ServerResponseData.regionList == null){}
        region = new Region("ranged_region", UUID.fromString("68356073-4B97-4A77-9B94-ED3D65718193"), null , null);
    }

    @Override
    protected void onResume() {
        super.onResume();

        SystemRequirementsChecker.checkWithDefaultDialogs(this);

        beaconManager.connect(new BeaconManager.ServiceReadyCallback() {
            @Override
            public void onServiceReady() {
                beaconManager.startRanging(region);
            }
        });


    }

    @Override
    protected void onPause() {

        super.onPause();

        beaconManager.stopRanging(region);


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    public  void showNotification(String title, String message) {
        Intent notifyIntent = new Intent(this, MainActivity.class);
        notifyIntent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivities(this, 0,
                new Intent[]{notifyIntent}, PendingIntent.FLAG_UPDATE_CURRENT);
        Notification notification = new Notification.Builder(this)
                .setSmallIcon(android.R.drawable.ic_dialog_info)
                .setContentTitle(title)
                .setContentText(message)
                .setAutoCancel(true)
                .setContentIntent(pendingIntent)
                .build();
        notification.defaults |= Notification.DEFAULT_SOUND;
        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(1, notification);
    }

}
