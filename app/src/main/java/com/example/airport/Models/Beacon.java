package com.example.airport.Models;

/**
 * Created by Dor on 25/05/16.
 */
public class Beacon {

    public String getBeaconId() {
        return beaconId;
    }

    public void setBeaconId(String beaconId) {
        this.beaconId = beaconId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getOpenLink() {
        return openLink;
    }

    public void setOpenLink(String openLink) {
        this.openLink = openLink;
    }

    public String getPingLink() {
        return pingLink;
    }

    public void setPingLink(String pingLink) {
        this.pingLink = pingLink;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getPushInterval() {
        return pushInterval;
    }

    public void setPushInterval(String pushInterval) {
        this.pushInterval = pushInterval;
    }

    public String getMajor() {
        return major;
    }

    public void setMajor(String major) {
        this.major = major;
    }

    public String getMinor() {
        return minor;
    }

    public void setMinor(String minor) {
        this.minor = minor;
    }

    String beaconId;
    String message;
    String openLink;
    String pingLink;
    String  startTime;
    String  endTime;
    String pushInterval;
    String major;
    String minor;

}
