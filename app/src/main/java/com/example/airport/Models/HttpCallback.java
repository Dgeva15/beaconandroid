package com.example.airport.Models;

/**
 * Created by Dor on 25/05/16.
 */
public interface HttpCallback {

    public void callback(int code, Object data, String message);

    public void callback (int code, String message);

}
