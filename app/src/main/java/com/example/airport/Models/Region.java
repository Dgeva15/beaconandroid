package com.example.airport.Models;

import java.util.List;

/**
 * Created by Dor on 25/05/16.
 */
public class Region {

    public List<Beacon> getBeacons() {
        return Beacons;
    }

    public void setBeacons(List<Beacon> beacons) {
        Beacons = beacons;
    }

    List<Beacon> Beacons;
    String identifier;
    String message;
    String pingLink;
    StringBuilder pushInterval;
    String regionId;
    String regionOpenLink;

    public String getIdentifier() {
        return identifier;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getPingLink() {
        return pingLink;
    }

    public void setPingLink(String pingLink) {
        this.pingLink = pingLink;
    }

    public StringBuilder getPushInterval() {
        return pushInterval;
    }

    public void setPushInterval(StringBuilder pushInterval) {
        this.pushInterval = pushInterval;
    }

    public String getRegionId() {
        return regionId;
    }

    public void setRegionId(String regionId) {
        this.regionId = regionId;
    }

    public String getRegionOpenLink() {
        return regionOpenLink;
    }

    public void setRegionOpenLink(String regionOpenLink) {
        this.regionOpenLink = regionOpenLink;
    }




}
