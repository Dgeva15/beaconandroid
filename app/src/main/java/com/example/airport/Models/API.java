package com.example.airport.Models;

import java.util.List;

/**
 * Created by Dor on 25/05/16.
 */
public class API {


    public static class ServerResponseData {
        public int code;
        public String message;
        public String data;
        public static List<Region> regionList;


        public List<Region> getRegionList() {
            if (regionList != null) {
                return regionList;
            } else {
                return null;
            }
        }

        public void setRegionList(List<Region> regionList) {
            this.regionList = regionList;
        }

//====================== CODE MSG DATA FORMAT============================

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public String getMessage() {

            return message;
        }

        public void setMessage(String message) {

            this.message = message;
        }

        public String getData() {
            return data;
        }

        public void setData(String data) {

            this.data = data;
        }


    }

    //=====================================

    public static int version;




    }




