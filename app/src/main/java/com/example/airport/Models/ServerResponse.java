package com.example.airport.Models;


/**
 * Created by Dor on 25/05/16.
 */
public class ServerResponse implements HttpCallback {

    public int Code;
    public String Message;
    public String Data;

    public String wholeJson;

    public int getCode() {
        return Code;
    }

    public void setCode(int code) {
        Code = code;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }

    public String getData() {
        return Data;
    }

    public void setData(String data) {
        Data = data;
    }

    @Override
    public void callback(int code, Object data, String message) {
        this.getData();
    }
    @Override
    public void callback(int code, String message) {
        this.getData();
    }

}
